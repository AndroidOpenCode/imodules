package dai.android.annon.module.b;

import android.app.Application;
import android.util.Log;

import dai.android.annon.annotation.executor;
import dai.android.annon.ibase.AbstractModule;

@executor(clazz = ModuleImpl.class)
public class ModuleImpl extends AbstractModule {

    public ModuleImpl(Application application) {
        super(application);
    }

    @Override
    public String name() {
        return "module_name_b";
    }

    @Override
    public void runnable() {
        Log.d("module", "this is module implement B");
    }
}
