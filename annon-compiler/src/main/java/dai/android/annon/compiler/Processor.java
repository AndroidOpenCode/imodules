package dai.android.annon.compiler;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import java.util.TreeSet;

import javax.annotation.processing.AbstractProcessor;
import javax.annotation.processing.Filer;
import javax.annotation.processing.Messager;
import javax.annotation.processing.ProcessingEnvironment;
import javax.annotation.processing.RoundEnvironment;
import javax.lang.model.SourceVersion;
import javax.lang.model.element.AnnotationMirror;
import javax.lang.model.element.Element;
import javax.lang.model.element.ExecutableElement;
import javax.lang.model.element.TypeElement;
import javax.lang.model.util.Elements;
import javax.tools.Diagnostic;

import dai.android.annon.annotation.caller;
import dai.android.annon.annotation.executor;
import de.greenrobot.common.ListMap;

public class Processor extends AbstractProcessor {

    private Filer mFiler;
    private Messager mMessager;
    private Elements mElements;

    private final ListMap<TypeElement, ExecutableElement> methodsByClass = new ListMap<>();

    @Override
    public boolean process(Set<? extends TypeElement> annotations, RoundEnvironment roundEnvironment) {

        for (TypeElement typeElement : annotations) {
            for (Element e : roundEnvironment.getElementsAnnotatedWith(typeElement)) {
                mMessager.printMessage(Diagnostic.Kind.NOTE, "Printing: " + e.toString());
                mMessager.printMessage(Diagnostic.Kind.NOTE, "Printing: " + e.getSimpleName());
                mMessager.printMessage(Diagnostic.Kind.NOTE, "Printing: " + e.getEnclosingElement().toString());
            }
        }


//        if (!roundEnvironment.processingOver()) {
//            Set<TypeElement> typeElements = getTypeElementsToProcess(
//                    roundEnvironment.getRootElements(),
//                    annotations);
//
//            for (TypeElement typeElement : typeElements) {
//                String packageName = mElements.getPackageOf(typeElement).getQualifiedName().toString();
//                String typeName = typeElement.getSimpleName().toString();
//                //ClassName className = ClassName.get(packageName, typeName);
//                mMessager.printMessage(Diagnostic.Kind.NOTE, "pkg name: " + packageName);
//                mMessager.printMessage(Diagnostic.Kind.NOTE, "typeName: " + typeName);
//            }
//        }

        return true;
    }

    private void collectModules(Set<? extends TypeElement> annotations, RoundEnvironment env) {
        for (TypeElement annotation : annotations) {
            Set<? extends Element> elements = env.getElementsAnnotatedWith(annotation);
            for (Element element : elements) {
                if (element instanceof ExecutableElement) {
                    ExecutableElement method = (ExecutableElement) element;
//                    if (checkHasNoErrors(method, messager)) {
//                        TypeElement classElement = (TypeElement) method.getEnclosingElement();
//                        methodsByClass.putElement(classElement, method);
//                    }
                }
            }
        }
    }

    @Override
    public synchronized void init(ProcessingEnvironment processingEnvironment) {
        super.init(processingEnvironment);

        mFiler = processingEnvironment.getFiler();
        mMessager = processingEnvironment.getMessager();
        mElements = processingEnvironment.getElementUtils();
    }

    @Override
    public SourceVersion getSupportedSourceVersion() {
        //return SourceVersion.RELEASE_7;
        return SourceVersion.latest();
    }

    @Override
    public Set<String> getSupportedAnnotationTypes() {
        return new TreeSet<>(
                Arrays.asList(
                        executor.class.getCanonicalName(),
                        caller.class.getCanonicalName()
                )
        );
    }


    static Set<TypeElement> getTypeElementsToProcess(Set<? extends Element> elements,
                                                     Set<? extends Element> supportedAnnotations) {
        Set<TypeElement> typeElements = new HashSet<>();
        for (Element element : elements) {
            if (element instanceof TypeElement) {
                boolean found = false;
                for (Element subElement : element.getEnclosedElements()) {
                    for (AnnotationMirror mirror : subElement.getAnnotationMirrors()) {
                        for (Element annotation : supportedAnnotations) {
                            if (mirror.getAnnotationType().asElement().equals(annotation)) {
                                typeElements.add((TypeElement) element);
                                found = true;
                                break;
                            }
                        }
                        if (found) break;
                    }
                    if (found) break;
                }
            }
        }
        return typeElements;
    }


}
