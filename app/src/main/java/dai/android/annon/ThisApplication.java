package dai.android.annon;

import android.app.Application;

import dai.android.annon.ibase.ModuleManager;

public class ThisApplication extends Application {

    @Override
    public void onCreate() {
        super.onCreate();

        ModuleManager.get().registerModules(this);
    }
}
