package dai.android.annon.module.a;

import android.app.Application;
import android.util.Log;

import dai.android.annon.annotation.executor;
import dai.android.annon.ibase.AbstractModule;

@executor(clazz = ModuleImpl.class)
public class ModuleImpl extends AbstractModule {

    public ModuleImpl(Application application) {
        super(application);
    }

    @Override
    public String name() {
        return "module_name_a";
    }

    @Override
    public void runnable() {
        Log.d("module", "this implement of module A");
    }
}
