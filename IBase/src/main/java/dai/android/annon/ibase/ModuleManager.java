package dai.android.annon.ibase;

import android.app.Application;

import dai.android.annon.annotation.caller;

public final class ModuleManager {

    private ModuleManager() {
    }

    private static ModuleManager manager = new ModuleManager();

    public static ModuleManager get() {
        return manager;
    }

    @caller(canWork = true)
    public void registerModules(Application application) {
    }

}
