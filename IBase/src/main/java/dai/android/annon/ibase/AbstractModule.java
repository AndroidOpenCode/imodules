package dai.android.annon.ibase;

import android.app.Application;

public abstract class AbstractModule implements IModule {

    protected final Application mApplication;

    public AbstractModule(Application application) {
        mApplication = application;
    }

}
